EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Crystal_GND3 Y?
U 1 1 640E64A1
P 4650 1750
F 0 "Y?" H 4650 2018 50  0000 C CNN
F 1 "Crystal_GND3" H 4650 1927 50  0000 C CNN
F 2 "" H 4650 1750 50  0001 C CNN
F 3 "~" H 4650 1750 50  0001 C CNN
	1    4650 1750
	1    0    0    -1  
$EndComp
Text Label 950  1350 2    50   ~ 0
XIN
Text Label 4950 1750 0    50   ~ 0
XIN
Wire Wire Line
	4800 1750 4950 1750
Wire Wire Line
	4500 1750 3650 1750
Wire Wire Line
	4650 2150 4650 1950
Wire Wire Line
	950  1350 1050 1350
Wire Wire Line
	950  1250 1050 1250
Text Label 4650 2150 3    50   ~ 0
DGND
Text Label 950  1250 2    50   ~ 0
TEST
$EndSCHEMATC
