https://commonmark.org/help/

### 2/6/2023

This is my first edit to my notebook.

I am writing this edit in the owen-notebook branch. I got to this branch by doing:
```
git pull
git switch owen-notebook
```

This branch is just for notebook work, and merging it should make no conflicts. I will do the following:
```
git status
git add owen/README.md
git commit -m "My message for the American People."
git push blitz-board owen-notebook
```

After that I can merge by going onto gitlab.engr.illinois.edu and doing:
```
sidebar -> "merge requests" -> "New merge request"
```
and following all instructions to merge "owen-notebook" (source) into "main" (target) without "Delete source branch."

Once I'm done with whatever git bash prompts me to do, I will be done. If the merge deleted my owen-notebook branch, I can make it again by doing:
```
git branch --list
git branch owen-notebook
```

### 2/7/2023

From meeting:

Start working on notebooks\
	* Document consistently\
	* Git notebook is good but have to be comfy with git\

Proposal due soon\
	* Make sure is not too wordy\
	* Consider that audience is technically adept\

Hardware comments\
	* Make less complex\
	* Less subsystems would be acceptable

### 2/14/2023

From meeting:

Hall effect sensors and stepper motors on 2-axis is sufficient in complexity according to Gruev

Proposal:\
Generally change everything to replace robot with 2-axis motors\
Problem statement needed\
Objective and background needs to be more complete according to requirements\
Too many requirements in some points in some in the requirements section\
Update requirements\
Update block diagram, email to Hanyin for confirmation before putting into design document\
	* Clarify that the control unit is a PCB with all of the connectors to other subsystems and connectors to raspberry pi\
If internet connection only is needed then use an ST or TI chip with internet functionality instead of raspberry pi\
	* If using stockfish consider a strong MCU, maybe a linux one. MCU surface mount and attached circuitry is a requirement\
Make the requirements more quantitative\
	* In design document need a means to verify each requirement\
Do tolerance requirements, make actual math involved\
	* Accuracy of hall effect sensors\
	* Precision of motors, precise enough to move pieces?\
	* Force of magnet vs strength of motors\
	* Power consumed by big components and system

Design review with presentation required\
	* Week of 2/27\
	* Sign up sheet will come at some point\
	* Diff TA and Professor reviewing\
PCB review with PCB almost-final draft required\
	* Week of 2/27

### 2/27/2023

From first design review:

High level requirements- put more abstract and high-level abilities\
    * Describe "parking lot" objectively, e.g. 20' x 6' area to right of board\
Block diagram\
    * Do other colors for arrows instead of dotted lines, easier to read\
Better define what a chess clock does\
More accuratein low-level requirements, more quantitative\
Find cheap hall effect sensors\
More evenly distribute talking points next time

### 3/6/2023

Looked at Hall Effect Sensor Datasheet:

If switches to one state, needs presence of opposite polarity magnetic field to switch back\
	* How would we do this with chess pieces?\
	* Why do they do that?\

Reed switches\
	* Cheaper\
	* Binary switch\
	* Will have to figure out how to multiplex

### 3/7/2023

From meeting:

Need first draft 3-8-23\
	* May have a third round for pcb\
	* Need to show up to tomorrow's one\

Need to modify design document\
	* Some additional feedback from Hanyin incoming by email or Canvas comment

### 3/15/2023

MSP430 symbol from TI is broken\
	* Need to desynchronize pieces to edit individually\
	* Need to ensure that symbol and footprint actually match datasheet since they gave you a broken one\

Using Single-Phase Electronic Watt-Hour Meter schematic by TI for reference\
	* Passives look good\
	* Annotate the pdf to see what circuits are for power meter and which are for microcontroller\
	* Do you need the EEPROM Array?\
	* What do you do with the aux power supplies?

### 3/21/2023

From meeting:

Lots of new PCB rounds\
	* Try to get in the third round at least\
	* Send to Hanyin if too late\

Check for JTAG programmers

### 3/26/2023

Almost finished with passives for MSP430\

For multiplexing:\
	* Using diodes so that inactive reed switches can't make 1 signal for 0 ones in same row\
	* Datasheet says that using same clock for shift and data registers is fine\
	* Use BJTs for LEDs, they're cheap\
	* Give one LED its own pin on the MCU, there are enough

### 4/4/2023

Starting layout for PCB\

Will be big, need 20 cm\

Find out if some isolated ground pads is ok

### 4/5/2023

Removed a buttons from control PCB because not enough space\
	* Put headers for two ports so those can be used for buttons\

Get everything in 10 cm\

Pass basic computer tests

### 4/10/2023

Get the MSP programmer from the lab\

Assemble the clocks

### 4/14/2023

Bing got the board fully soldered\

TI CCS throws vague error, doesn't work

### 4/20/2023

I2C not working for clocks\
	* Signal looks weird on oscilloscope\
	* It's not holding the signal high for some reason\

Control circuit kinda works, now can program MSP through MSP Box\
	* Need to hold ribbon cable so there's tension

