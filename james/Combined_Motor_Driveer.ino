// Include the Arduino Stepper and AH_EasyDriver Library
#include <Stepper.h>
#include <AH_EasyDriver.h>
//#include <AccelStepper.h>

//Declare pin functions on RedBoard
//#define EM 13

// Declare variables
char user_input;
const int stepsPerRevolution = 400;
const int step = 200;
const int step2 = 1375;
const int RES = 80; // RES -> RESOLUTION per full revolve

// Create Stepper library instance for L298N
Stepper stepper1(stepsPerRevolution, 8, 9, 10, 11);
// Create AH_EasyDriver library instance for Easy Driver
// AH_EasyDriver(int RES, int DIR, int STEP, int MS1, int MS2, int SLP, int ENABLE, int RST)
AH_EasyDriver stepper2(RES, 3, 2, 4, 5, 6, 7, 12); 

void setup()
{
  
  //pinMode(EM, OUTPUT);
  //resetEDPins(); //Set step, direction, microstep and enable pins to default states
  
  // Setup L298N
  stepper1.setSpeed(52.5); // Set the speed at 52.5 rpm
  // Setup Easy Driver
  stepper2.sleepOFF(); // Set Sleep mode OFF
	stepper2.resetDriver();
  stepper2.enableDriver(); 
  stepper2.setMicrostepping(3); // MODE 3 -> 1/8 microstep
  //stepper2.setSpeedRPM(52.5); // RPM , rotations per minute
  stepper2.setSpeedHz(1400); // Hz, steps per second
  // Initialize the serial port
  Serial.begin(9600);
	Serial.println("Begin motor control");
  //Print function list for user selection
  Serial.println("Enter the moving piece:");
  Serial.println();
}

// Main loop
void loop() {
  while (Serial.available()) {
    user_input = Serial.read(); // Read user input and trigger appropriate function
    if (user_input == '1')
    {
      RookForwardStep();
    }
    else if (user_input == '2')
    {
      RookBackwardStep();
    }
    else if (user_input == '3')
    {
      RookRightStep();
    }
    else if (user_input == '4')
    {
      RookLeftStep();
    }
    else if (user_input == '5')
    {
      BishopForwardRightStep();
    }
    else if (user_input == '6')
    {
      BishopBackwardLeftStep();
    }
    else if (user_input == '7')
    {
      BishopForwardLeftStep();
    }
    else if (user_input == '8')
    {
      BishopBackwardRightStep();
    }
    else if (user_input == '9')
    {
      KnightForwardForwardRightStep();
    }
    else if (user_input == '0')
    {
      KnightBackwardBackwardLeftStep();
    }
    /*else if (user_input == '9')
    {
      PieceTakingStep();
    }*/
    else
    {
      Serial.println("Waiting for next move");
      Serial.println();
    }
  }
}

/*
// Rook Moving Forward with Electromagnet function
void MagnetStep()
{
  Serial.println("Rook moves 2 blocks forward");
  digitalWrite(EM, LOW); // Turn on the electromagnet
	stepper1.step(-step*2); // Positive -> Backward
  digitalWrite(EM, HIGH); // Turn off the electromagnet
  Serial.println("Enter next move");
  Serial.println();
}
*/

// Rook Moving Forward function
void RookForwardStep()
{
  Serial.println("Rook moves 2 blocks forward");
  //digitalWrite(EM, LOW); // Turn on the electromagnet
	stepper1.step(-step*1); // Positive -> Backward
  //digitalWrite(EM, HIGH); // Turn off the electromagnet
  Serial.println("Enter next move");
  Serial.println();
}

// Rook Moving Backward function
void RookBackwardStep()
{
  Serial.println("Rook moves 2 blocks backward");
	stepper1.step(step*1); // Positive -> Backward
  Serial.println("Enter next move");
  Serial.println();
}

// Rook Moving Right function
void RookRightStep()
{
  Serial.println("Rook moves 2 blocks right");
  stepper2.move(step2*1); // Positive -> Right
  Serial.println("Enter next move");
  Serial.println();
}

// Rook Moving Left function
void RookLeftStep()
{
  Serial.println("Rook moves 2 blocks left");
  stepper2.move(-step2*1); // Positive -> Right
  Serial.println("Enter next move");
  Serial.println();
}

// Bishop moving forward-right function
void BishopForwardRightStep()
{
  Serial.println("Bishop moves 2 blocks forward-right diagonally");
	stepper1.step(-step*2); // Positive -> Backward
  stepper2.move(step2*2); // Positive -> Right
  Serial.println("Enter next move");
  Serial.println();
}

// Bishop moving backward-left function
void BishopBackwardLeftStep()
{
  Serial.println("Bishop moves 2 blocks backward-left diagonally");
	stepper1.step(step*2); // Positive -> Backward
  stepper2.move(-step2*2); // Positive -> Right
  Serial.println("Enter next move");
  Serial.println();
}

// Bishop moving forward-left function
void BishopForwardLeftStep()
{
  Serial.println("Bishop moves 2 blocks forward-left diagonally");
	stepper1.step(-step*2); // Positive -> Backward
  stepper2.move(-step2*2); // Positive -> Right
  Serial.println("Enter next move");
  Serial.println();
}

// Bishop moving backward-right function
void BishopBackwardRightStep()
{
  Serial.println("Bishop moves 2 blocks backwardward-right diagonally");
	stepper1.step(step*2); // Positive -> Backward
  stepper2.move(step2*2); // Positive -> Right
  Serial.println("Enter next move");
  Serial.println();
}

// Knight moving forward-forward-right function
void KnightForwardForwardRightStep()
{
  Serial.println("Knight moves 2 blocks forward and 1 block right");
	stepper1.step(-step*2); // Positive -> Backward
  stepper2.move(step2*1); // Positive -> Right
  Serial.println("Enter next move");
  Serial.println();
}

// Knight moving backward-backward-left function
void KnightBackwardBackwardLeftStep()
{
  Serial.println("Knight moves 2 blocks backward and 1 block left");
	stepper1.step(step*2); // Positive -> Backward
  stepper2.move(-step2*1); // Positive -> Right
  Serial.println("Enter next move");
  Serial.println();
}

// Piece taking function
void PieceTakingStep()
{
  Serial.println("Knight takes a piece then brings the piece to parking space");
	stepper1.step(-step*2); // Positive -> Backward
  stepper2.move(step2*1); // Positive -> Right
  delay(500);
  stepper2.move(-step2*4); // Positive -> Right
  Serial.println("Enter next move");
  Serial.println();
}

/*
//Reset LED switch pins to default states
void resetEDPins()
{
  digitalWrite(EM, HIGH);
}
*/

