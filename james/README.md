# James Tang's Senior Design Lab Notebook
>  failing to plan is planning to fail

## In-Lab Experimentation and Findings
#### 02/06/2023

We tried to figure out how we are going to use GitLab as our lab notebook, and the following steps should be the future workflow of updating the notebook. I referred to the git guide on GitHub. We also got familiar with the markdown syntax.

1. `git pull` to update my local branch before making any changes
2. `git checkout james-notebook` to switch from the main branch to my branch
3.  `git status`  to track the changed files that are staged for the next commit
4. `git add [filename]` or `git add all .` to choose which files to commit
5. `git commit -m "[descriptive commit message]"` 
6. `git push blitz-board james-notebook`  to uploads local branch commits to the corresponding remote branch

Useful Websites:
- https://github.com/git-guides
- https://commonmark.org/help/

#### 02/10/2023 (Friday)

After talking with the machine shop about our robot car design, they think the design is not only too complicated for a one-semester project but also too physically challenging. It is almost impossible to fit an RC car under the chessboard because of several reasons, including the size of the motor and the mobility of the RC car. Therefore, we can no longer use the robot car design and have to switch to a dual-axis motor design instead, so we grabbed a chess board that was built and used as a senior project before.

#### 02/14/2023 (Tuesday)

We discussed how to implement the dual-axis design. We have to redesign many parts of the project.

#### 02/27/2023 (Monday)

We worked on the design document and prepared for the design review. In the design review, Professor Mironenko gave us a few suggestions on how to change our design document. First, be more specific about the high-level requirements. For example, replace moving the chess piece "accurately" with an actual number. Second, switch dotted lines with other types of lines. Third, allow every member to talk more evenly in the presentation next time.

#### 03/28/2023 (Tuesday)

We worked on the schematic of the PCB. We connected all the MCU ports, determined the passives' values, and finalized all the connections between units.

#### 04/03/2023 (Monday)

We tested the reed switches and noticed that the orientation of the reed switches matters. We tried moving a permanent magnet toward the reed switches placed at a different angle, and their ability to detect magnetic fields will differ significantly. The resistance of the switch is 0.2 ohm. 

Aside from the reed switches, we conducted some tests on the electromagnet. The electromagnet is too weak to pull the metallic chess piece with the board top in between. The permanent magnet is so strong that it is always attached to the electromagnet, whether the electromagnet is on or off. As a result, we decided to find some magnetic metals that can be used as a chess piece, and reed switches are now only triggered by the electromagnet since there are no longer magnets on chess pieces. Due to the change, we will now have to rely more on the software to determine the location of each piece. We are aiming for some small, light, and magnetic metal to replace the permanent magnet, and Nick believes he could find some iron washers in his house.

#### 04/04/2023 (Tuesday)

We worked on the finalized the layout of the PCB. We grouped the footprints into different based on their functions and tried to limit the board size to 100mm by 100mm. Later we concluded that it was impossible to squeeze all the necessary components into the 100mm by 100mm board, so we separated the board into two boards.

#### 04/05/2023 (Wednesday)

We tested whether the iron washer he brought could be used as a chess piece. Unfortunately, the 5 V electromagnet still couldn't grab the iron washer. We then start thinking about how the board top can be modified to accommodate this situation. We made a draft for board top modification and talked with the machine shop. We were told the idea wouldn't work, and we discussed multiple options. After the talk, we had something in mind but needed to talk with Owen to confirm the technical details, so we scheduled an appointment tomorrow with the machine shop.

#### 04/06/2023 (Thursday)

After discussing with Owen, we decided on the final modification plan and drew lines on the board top as a visual aid for the machine shop. The modification includes the following. First, we are drilling channels on the inner side of the board top to embed the reed switches and the wires. However, we will have to remove the LEDs because there is not enough space left. Second, we are switching out the 5 V electromagnet with a larger 12 V electromagnet, which will be powered by the 12 V DC power supply. We then confirmed the adjustment with the machine shop and expect to receive the modified board top by next Wednesday. After the board top is modified, the machine shop will work on the electromagnet.

#### 04/07/2023 (Friday)

We tested the stepper motor. We confirmed the two phases of the three stepper motors with an LED. We randomly connected two of the four wires to the LED and spun the motor. If the LED lights up, we know the two wires are the wires for the same phase. We also fixed the unusable wires on the motors by removing the old ones and soldering new ones to them.

#### 04/10/2023 (Monday)

We started working on the software aspect of the project. We looked into some codes that can be used for the microcontroller.

#### 04/17/2023 (Monday)

The machine shop has completed the modifications to our chess board, including drilling channels on the board top for the reed switches, replacing the original 5 V electromagnet with a larger 12 V one, and adding some height to the pillars to accommodate the increase in the height of the electromagnet. Nick and I started to embed the reed switches into the board top.

#### 04/18/2023 (Tuesday)

We continued to embed all the reed switches into the board top. After embedding all the reed switches, we started soldering the positive and negative connections. We have to extrude the wires so that we are able to solder the wires to the reed switches together. Since extruding 32 wires is quite some work, we tried several ways so we could do it most effectively.

#### 04/19/2023 (Wednesday)

We finished soldering the wires to the reed switches. However, we are not sure if we will actually use the reed switches due to the microcontroller not functioning properly.

#### 04/20/2023 (Thursday)

I searched online to figure out the Arduino code for the stepper driver and found several resources. Still, they are not entirely compatible with the EasyDriver that we are using. Then I noticed there are some sample codes to use as a foundation in the hookup guide of the EasyDriver. There are also some resources, including some basic projects, to refer to on the website of Schmalz Haus, the person who designed the EasyDriver. I modified the code, including setting up the right step wave and step size. The original setting was 1 kHz and full-step, and I made it 1.4 KHz and half-step. 

Useful Websites: 
- https://learn.sparkfun.com/tutorials/easy-driver-hook-up-guide?_ga=2.58118894.539045501.1683214344-1407357720.1675188605&_gac=1.48758228.1682615404.Cj0KCQjw_r6hBhDdARIsAMIDhV950ZJ5cCMdTuhzl92RD44Ogy-TA5kHr2Eo16YnMSeMdD8s0BdzsecaAmkEEALw_wcB
- https://www.schmalzhaus.com/EasyDriver/

#### 04/21/2023 (Friday)

We tried running one of the stepper motors with the code I had yesterday. The motor wasn't moving and was making some high-frequency noises. We thought the rails or the amotor might be rusty, so we went downstairs to talk to the machine shop to see if we could apply some lube. Greg suggested that there might be something wrong on our side. It turned out that the problem is becauses our step size is too big. As we shifted from half-step to quarter-step, things worked a lot better. There are fewer noises, and the motors move a lot smoother. However, the motor might be moving a bit too fast, and we are afraid that the motor couldn't move the chess piece at this speed. We continued to explore the eighth step, and we concluded that this was the step size we were going for. We tested which frequency we are operating at eighth step, starting from 1 kHz to 2 kHz in 200 Hz increments, and we determined that 1.4 kHz at eighth step is the best operation setting.

After that, we connected the EasyDriver to the other two stepper motors. As in the previous testing, we used the 12 V DC power supply to power the motors. It didn't quite work, and the motors did not move in the first time because we did not notice that we had to reverse the phases of one of the motors so they could turn in the same direction. As we got the two motors to work, we also noticed that the voltage dropped from 12 V to approximately 7.5 V. I think it might be caused by two motors running simultaneously having reached the current and power limit of the power supply. Hence, the power supply has to reduce the voltage. We tested the electromagnet as well. It was able to grab the chess piece as the motors operate in 1.4 kHz at eighth step. Plan on finalizing the piece-moving unit tomorrow.

#### 04/23/2023 (Sunday)

We continued to modify the Arduino code and planned to finish building the entire piece moving today. As we finished soldering the other EasyDriver and tried to do some testing on the breadboard, we found out that the EasyDriver was not working. We probed all the pins of the EasyDriver and listed all the possibilities, including bad breadboard connection and poor soldering technique. However, we still can get the driver working, even if we used the same circuit the other EasyDriver was connected to. So we might have to try to control three stepper motors with one EasyDriver, which I can't see how is possible for now.

Good news! I found two motor drivers, L298N and L293D, from somewhere deep in my cabinets. The L298N motor driver can drive one stepper motor, and the L293D Motor Driver Shield can drive two stepper motors. I researched the two drivers and figured out how they operate and how they can work with the EasyDriver. I also got some testing codes written, and I plan on testing the new drivers tomorrow.

Useful Websites
- https://lastminuteengineers.com/stepper-motor-l298n-arduino-tutorial/
- https://lastminuteengineers.com/l293d-motor-driver-shield-arduino-tutorial/

#### 04/24/2023 (Monday)

We decided to use the L298N motor driver to control one motor and the EasyDriver to control the other two. We soon discovered that the two drivers' control mechanisms are very different. The EasyDriver allows us to directly set the period of the step wave, the cycles of the step wave, and the step size. On the other hand, the L298N motor driver only allows up to set the number of steps and steps per revolution. We decide to use two Arduinos two control two motor drivers first. It worked, but it doesn't seem like a good idea since it requires two people to control the motor simultaneously. I think I can make the two interconnect through some algebraic manipulation since the two mechanisms are fundamentally the same thing, but it would be a pain, and the code would be pretty messy. 

I later found an Arduino stepper library for the EasyDriver module. It turns the controlling methods of the EasyDriver into the mechanism of the L298N motor driver. Even though it required some tweaking, I eventually got the code to work. As a side note, the library formatted the pin sequence in a function a bit differently compared to the original design, so we connected the wrong pins when we did the first testing. We even thought the L298N motor driver was faulty. Luckily, I identified the problem and switched the pin number on the code. In the end, we are able to get the piece-moving unit working.

Useful Websites
- https://forum.arduino.cc/t/arduino-stepper-library-for-easydriver-module/103497
- https://github.com/ramonidea/Icing-Food-printer/tree/master/AH_EasyDriver_20120512/AH_EasyDriver

#### 04/25/2023 (Tuesday)

I made some final adjustments to the Arduino codes. Nick and I first made sure how many steps of the motors equal one block on the chess board. It allows me to design the different movements of the motors further. I hard-coded several cases to imitate the moves of different pieces. For example, the rook moves two blocks forward, the bishop moves three blocks diagonally in the forward-right direction, and the knight moves two blocks right and one block forward. I also imitated some piece-taking movements. For example, a bishop takes another piece and moves the taken piece to the parking space on the right of the chess board. I even tried to move the two motors simultaneously but could not figure out how to do so before the final demo. Owen helped me with the data types of the Arduino because I wanted to make the commands that users enter more intuitive. For example, instead of pressing "1" for the rook to move two blocks forward, the user can press "RF," in which R indicates the type and "F" stands for direction.
