# Senior Design Notebook- Blitz Board - Nick Bingenheimer
***
## Completed Writing Assignments **GROUP**
***

***
## Completed Writing Assignments **INDIVIDUAL**
***

***
## In-Lab Experimentation and Findings
***
Wed, 04/05/2023, ~12:30PM:

Began testing on the recieved product from the ECE Machine Shop. Looking to ensure operation of electromagnetic field can breach the board top, the orientation needed for the Reed switches to detect field from the permanent magnets, if the Reed switches can detect field through the board top, and operation of the motors. This needs to be done so that physical modifications to the mechanical designs of the board can be done by the ECE Machine shop in a reliable amount of time.

First discovered that using a permanent magnet that resides within pieces as a means of being able to grab chess pieces with the electromagnet was not feasible as the permanent mangent would become bonded to the electromagnet despite it not having power.

Then discovered that chess pieces would need small, flat pieces of iron as the base so that the elecromagnet could grab **AND** let go of chess pieces. 

With this new revelation and some iron washers, they ability of the electromagnet to pull the iron washer across the board was tested with little result. The 5V electromagnet was not able to produce enough field to manipulate objects on the opposite side of the board top.

Cease Testing for the day, 04/05/2023, ~2:30PM

Thursday, 04/06/2023, ~11AM:

Began more testing on the board. Since Reed Switches only sense the discrete presence of magnetic field, they would not be able to detect all pieces if permanent magnets were no longer being placed in the chess pieces. This posed a question of how the software must now adapt to the physical limitations of the Board. Still, they could detect the field from the electromagnet when in the correct orientation. 

As per yesterday's test, a bigger electromagnet was needed. ECE Machine Shop provided a larger 12V magnet that could still be implemented with our current power unit designs. With this larger electromagnet, the Reed switches could reliably sense the field from the electromagnet though the board top. This was confirmed by passing 12VDC through the DC electromagnet and observing if resistance dropped to a negligible amount across the Reed switch, which would otherwise read as OPEN. This was exactly what was observed. A success indeed. 

Still, the board top required modifications for the Reed switches to be embedded within the boardtop, and Communication LEDs were abandoned from the design at the behest of the ECE Machine shop. The plan made with the machine shop was to mill out grooves for the Reed Switches and jumper wires to be places such that the connections could be reached from all sides of the board, and the Reed Switches would not move. This was handed off to the ECE Mahcine Shop.

Cease Testing and Experimentation for the Day, 04/06/2023, ~12:30PM

Friday, 04/07/2023, ~10AM:

Began testing of motors. Initial testing was short, and only included ensuring the two phases of each motor could be separated for future use. Some issues with the leads of one motor were found, cutoff, and new connections were soldered in place of the old. Motor torque ratings exceeded the required amount needed to move iron washer, chess piece, and electromagnet, at an estimated cost of speed. More testing will be done when new electromagnet is installed and boardtop is finished. 

Cease Testing and Experimentation for the Day, 04/07/2023, ~12PM

Monday, 04/10/2023, ~1PM:

Late start to weekly meeting, began software design and split up some tasks that small portions of the coding effort could be divided into. I'll take the operation of the microcontroller controlling the easydriver chips, as well as control of the electromagnet. Need to discover how to generate a square-wave of desired frequency in order to drive the step pin of the EasyDriver_v4.5. This will set the step frequency of the motors and must be constant for all three motors. Additionally, need to research how to handle multiple different devices, being the two motor drivers, they are routed to different ports anyways, but will need to take in their own number of steps and directions. Will likely need to set a priority for which motor moves first as well. 

Cease Weekly meeting for the Day, 04/10/2023, ~1:45PM

Friday, 4/21/2023, ~10AM:

It has certainly been a long week. Much progress has been made on the Blitz Board, although I don't think it will be blitzing persay. Throughout the week, James and I have finished putting together most of the mechanical parts of the board. This included inserting the Reed Switches into the boardtop, soldering leads to them to create a wire harness, and extruding the leads to the ends of the board. The ECE Machine Shop was able to drill out grooves into the board for the Reed Switches to be placed as well as install a larger electromagnet. We were close to finishing the entire boardtop, but left some of the leads of the Reeds swithces short as we don't think we'll have time to meticulously lay out functionality for the entire board to be sensing.

Additionally, Owen and myself have made much progress on finishing the PCB as the order came in. Only a few parts are left and they may be leftout all together in the interest of time. 

Finally, after much labor in putting as much of the board together as possible, James and I were able to set aside a few hours today to test out how the motors worked with the new electromagnet. **LINK PNG FILES???** The following files display some information on how the linear speed of the electromagnet as driven by the motors changed with step frequency, as well as displaying this information under different control regimes. We found that the motor worked poorly and was very lound and shaky when driven under full and hal-step operation. Performance was improved but still noisy under quarter step operation, and honestly almost moved too fast. We found the motors worked best under the 8th step operation, the movement was quiet, swift and easy to control. This **MUST** be taken into account when writing code to drive the stepper motors. 
Additionally, we found that the EasyDriver_v4.5 was capable of driving two motors at once, which works well for us since we suddenly found the two-axis motor system actually has three motors. The only assumption to proper operation is that one motors phases must be flipped in order for it to rotate in the opposite direction of its twin, only this way will the two motors move the electromagnet in one direction. Before this change was made, each motor attempted to move the rail the magnet sat on in different direction and caused the system to stall-out. The only drawback to driving two motors with one driver circuit is that they will move slower due to having to split power between two motors.
Again, this is unfortunate that speed is no longer a priority due to physical and time time limitation, but we will do what we must.
***
## Electrical/Mechanical Design Considerations and Modifications
***
Board top needs to be thinner or larger electromagnet is requred -ECE Mahcine Shop, 02/17

Multiplexing will be the most difficult hardware task, will require huge modifications to the board -Weekly Meeting, 02/20

Hall effect sensors mentioned in original design are abandoned due to lack of space, money, and inability to multiplex analog signals according to Owen -Note to self 02/31

Can two motors be driven by the same easydriver chip? -Myself, 03/08
yes they can!-04/21

All components ordered, need to design PCB, footprint for easy driver and symbol must be made in order to do so -Weekly meeting 03/20

Original play-space was designed as 18"x22", this has reduced significantly due to need for larger electromagnet. New play space is set to 18"x18", with 1" border around the entire board, making play-space grid a 16"x16" inch space for 8x8 grid of 2" squares. This design handed off to machine shop for boardtop modifications -Note to self, 04/06

LEDs that were designed to be embedded into the chess boardtop must be abandoned, ECEmachine shop says its impossible and they are out of time -Note to self, 04/06

Sensing will be much different that originally designed, Reed switches will only sense Human Player's Pieces, MCP will simply know its own moves and pieces locations. Human Player Pieces will have weak permanent electromagnets in them, must not interfere with movement of the computer's pieces **(8^0)** -
***
## Research and Contributions to PCB Design
***

***
## Research and Contributions to Programming Microprocessor
***

***
